import sys
import gi
import logging
import datetime
import subprocess, shlex, psutil

from datetime import datetime

gi.require_version("GLib", "2.0")
gi.require_version("GObject", "2.0")
gi.require_version("Gst", "1.0")

from gi.repository import Gst, GLib, GObject
from std_msgs.msg import String, Empty
from data_record_util.srv import *



import rospy
from threading import Lock
from yaml import load
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


class DataRecordUtil:

    def __init__(self) -> None:
        self.running = False

        # Initialize GStreamer
        Gst.init(sys.argv[1:])
        logging.basicConfig(level=logging.DEBUG, format="[%(name)s] [%(levelname)8s] - %(message)s")
        self.logger = logging.getLogger(__name__)
        self.source = Gst.ElementFactory.make("v4l2src", "source")
        self.sink = Gst.ElementFactory.make("filesink", "sink")

        self.filter = Gst.ElementFactory.make("avimux", "filter")
        self.filter2 = Gst.ElementFactory.make("avimux", "filter2")

        self.source2 = Gst.ElementFactory.make("v4l2src", "source2")
        self.sink2 = Gst.ElementFactory.make("filesink", "sink2")

        self.pipeline = Gst.Pipeline.new("test-pipeline")

        if not self.pipeline or not self.source or not self.sink:
            self.logger.error("Not all elements could be created.")
            sys.exit(1)


        # Build the pipeline
        self.pipeline.add(self.source)
        self.pipeline.add(self.source2)
        self.pipeline.add(self.filter)
        self.pipeline.add(self.filter2)
        self.pipeline.add(self.sink)
        self.pipeline.add(self.sink2)
        if not self.source.link(self.filter):
            self.logger.error("Elements could not be linked.")
            sys.exit(1)
        if not self.source2.link(self.filter2):
            self.logger.error("Elements (2) could not be linked.")
            sys.exit(1)
        if not self.filter.link(self.sink):
            self.logger.error("Elements could not be linked.")
            sys.exit(1)
        if not self.filter2.link(self.sink2):
            self.logger.error("Elements (2) could not be linked.")
            sys.exit(1)

        

    def run(self):
        '''
        function to run the node, setup subscribers, and setup services
        '''
        rospy.init_node('data_util')

        if rospy.has_param('~cameras'):
            self.cameras = rospy.get_param('~cameras')
        if rospy.has_param('~full_path'):
            self.full_path = rospy.get_param('~full_path')
        # start the recording of the data with prefixes to file names
        rospy.Service("~start_record",
                      StartRecord, self.start_record)
        # Change the state of the ghost track to start collecting, stop collecting, or reset the collection
        rospy.Service("~stop_record",
                      StopRecord, self.stop_record)
        if(self.running):
            print("asdf")
            
        
        rospy.spin()
    def start_record(self, req):
        now = datetime.now()
        
        self.source.set_property("device", self.cameras[0])
        self.source2.set_property("device", self.cameras[1])
        print("yo")
        rospy.loginfo(self.cameras[0])
        rospy.loginfo(self.cameras[1])
        self.sink.set_property("location", self.full_path + req.filePrefix.data + "camera1" + now.strftime("%d/%m/%Y%H:%M:%S"))
        self.sink2.set_property("location", self.full_path + req.filePrefix.data + "camera2" + now.strftime("%d/%m%Y%H:%M:%S"))
        
        rospy.loginfo(self.full_path + req.filePrefix.data)  
        


        self.running = True
        ret = self.pipeline.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("Unable to set the pipeline to the playing state.")
        rospy.loginfo(self.full_path + req.filePrefix.data + "_rosbag" +now.strftime("%d-%m-%Y-%H:%M:%S"))
        self.command = "rosbag record -O " + self.full_path + req.filePrefix.data + "-rosbag-" +now.strftime("%d-%m-%Y-%H:%M:%S") +" -a"
        self.command = shlex.split(self.command)
        self.rosbag_proc = subprocess.Popen(self.command)
        return StartRecordResponse(String("Recording"))
    def stop_record(self, req):
        for proc in psutil.process_iter():
            if "record" in proc.name() and set(self.command[2:]).issubset(proc.cmdline()):
                proc.send_signal(subprocess.signal.SIGINT)

        self.rosbag_proc.send_signal(subprocess.signal.SIGINT)

        ret = self.pipeline.set_state(Gst.State.NULL)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("Unable to set the pipeline to the playing state.")
        StopRecordResponse("Stopped Recording")

def main():
    util = DataRecordUtil()
    util.run()


if __name__ == '__main__':
    main()
