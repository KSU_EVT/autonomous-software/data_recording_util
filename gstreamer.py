#!/usr/bin/env python3
import sys
import gi
import logging

gi.require_version("GLib", "2.0")
gi.require_version("GObject", "2.0")
gi.require_version("Gst", "1.0")

from gi.repository import Gst, GLib, GObject

logging.basicConfig(level=logging.DEBUG, format="[%(name)s] [%(levelname)8s] - %(message)s")
logger = logging.getLogger(__name__)

# Initialize GStreamer
Gst.init(sys.argv[1:])

# Create the elements
source = Gst.ElementFactory.make("v4l2src", "source")
sink = Gst.ElementFactory.make("filesink", "sink")

filter = Gst.ElementFactory.make("avimux", "filter")
filter2 = Gst.ElementFactory.make("avimux", "filter2")

source2 = Gst.ElementFactory.make("v4l2src", "source2")
sink2 = Gst.ElementFactory.make("filesink", "sink2")

# Create the empty pipeline
pipeline = Gst.Pipeline.new("test-pipeline")

if not pipeline or not source or not sink:
    logger.error("Not all elements could be created.")
    sys.exit(1)


# Build the pipeline
pipeline.add(source)
pipeline.add(source2)
pipeline.add(filter)
pipeline.add(filter2)
pipeline.add(sink)
pipeline.add(sink2)

if not source.link(filter):
    logger.error("Elements could not be linked.")
    sys.exit(1)
if not source2.link(filter2):
    logger.error("Elements (2) could not be linked.")
    sys.exit(1)
if not filter.link(sink):
    logger.error("Elements could not be linked.")
    sys.exit(1)
if not filter2.link(sink2):
    logger.error("Elements (2) could not be linked.")
    sys.exit(1)


# Modify the source's properties
source.set_property("device","/dev/video0")
source2.set_property("device","/dev/video1")
sink.set_property("location","xyzasdf.avi")
sink2.set_property("location","xyzasdf2.avi")
# Can alternatively be done using `source.set_property("pattern",0)`
# or using `Gst.util_set_object_arg(source, "pattern", 0)`

# # Start playing

ret = pipeline.set_state(Gst.State.PLAYING)

if ret == Gst.StateChangeReturn.FAILURE:
    logger.error("Unable to set the pipeline to the playing state.")
    sys.exit(1)


# wait until the enter key has been pressed then kill the stream

pipeline.set_state(Gst.State.NULL)
